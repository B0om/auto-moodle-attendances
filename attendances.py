#!/usr/bin/env python3
# coding: utf-8

import getpass
import os
from urllib.parse import parse_qs, urlparse

import requests
import yaml
from bs4 import BeautifulSoup

MOODLE_URL = 'https://moodle.univ-ubs.fr'

MOODLE_LOGIN_URL = f'{MOODLE_URL}/login/index.php?authCAS=CAS'
CAS_LOGIN_URL = 'https://cas.univ-ubs.fr/login'

ATTENDANCE_COURSES_URL = {
    's5': f'{MOODLE_URL}/mod/attendance/view.php?id=155079',
    's6': f'{MOODLE_URL}/mod/attendance/view.php?id=155081',
}


def login_to_moodle(username, password):
    session = requests.Session()

    resp = session.get(CAS_LOGIN_URL, params={'service': MOODLE_LOGIN_URL})
    html = BeautifulSoup(resp.content, 'html.parser')
    csrf_token = html.find('input', {'name': 'execution'})['value']

    resp = session.post(CAS_LOGIN_URL, data={
        'username': username,
        'password': password,
        'execution': csrf_token,
        '_eventId': 'submit',
        'submit': 'LOGIN',
    })
    html = BeautifulSoup(resp.content, 'html.parser')
    error = html.select('.alert-danger')
    if error:
        error_msg = error[0].find('span').text
        print(f'CAS Error: {error_msg}')
        exit(1)

    return session


def submit_attendance(session, attendance_url):
    params = parse_qs(urlparse(attendance_url).query)
    resp = session.post(attendance_url.split('?')[0], data={
        'sessid': params['sessid'],
        'sesskey': [params['sesskey'], params['sesskey']],
        '_qf__mod_attendance_student_attendance_form': 1,
        'mform_isexpanded_id_session': 1,
        'status': 41,
        'submitbutton': 'Enregistrer',
    })
    if resp.status_code != 404:
        print(f'[+] Attendance OK - {attendance_url}')
    else:
        print(f'[X] Attendance ERROR - {attendance_url}')


def submit_all_attendances(session, semester='s5'):
    resp = session.get(ATTENDANCE_COURSES_URL[semester])
    html = BeautifulSoup(resp.content, 'html.parser')
    courses = html.select('table.generaltable')[0].select('tbody tr')
    if courses:
        for course in courses:
            link = course.select_one('td a')
            if link:
                submit_attendance(session, link['href'])


def get_config_from_stdin(config={}):
    """Get config from stdin; don't ask for fields already existing in config.

    Args:
        config (dict): dict with already existing config.
    """
    if not config.get('username'):
        config['username'] = input('  ~ Username: ')
    if not config.get('password'):
        config['password'] = getpass.getpass('  ~ Password: ')
    if not config.get('semester'):
        config['semester'] = input('  ~ Semester (s5, s6) [s5]: ') or 's5'
    return config


def get_config(config_file='config.yml'):
    """Load config from existing file then ask for missing fields.

    Example of config file:
        username: e1500xxx
        password: xxxxxxxxxxx
        semester: s5
    """
    if os.path.exists(config_file):
        print(f'  ~ Loading config from {config_file}')
        with open(config_file) as f:
            config = yaml.safe_load(f.read())
    return get_config_from_stdin(config)


if __name__ == '__main__':
    print('[*] AUTOMATIC MOODLE ATTENDANCES SUBMISSION')
    print('  ~ This script aims to autosubmit attendance for each current session on Moodle.\n')

    print('[-] Config')
    config = get_config()

    print('\n[+] Login to Moodle')
    session = login_to_moodle(config['username'], config['password'])

    print('\n[+] Submitting attendances')
    submit_all_attendances(session, config['semester'])

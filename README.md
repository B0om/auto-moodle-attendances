# Automatic Moodle attendances submission

## Install dependencies

```shell
$ python3 -m pip install -r requirements.txt --user
```

## Configuration

You can create a config file to avoid being asking for credentials and semester every time.

```shell
$ cp config.yml.template config.yml
```

## Run

```shell
$ python3 attendances.py
```
